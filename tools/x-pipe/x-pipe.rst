

.. index::
   pair: CLI; X-pipe

.. _xpipe:

===========================================================================
**x-pipe Your entire server infrastructure at your fingertips**  |xpipe|
===========================================================================

- https://github.com/xpipe-io/xpipe


XPipe is a new type of shell connection hub and remote file manager that allows
you to access your entire server infrastructure from your local machine.

It works on top of your installed command-line programs and does not require
any setup on your remote systems.

XPipe fully integrates with your tools such as your favourite text/code editors,
terminals, shells, command-line tools and more. The platform is designed to
be extensible, allowing anyone to add easily support for more tools or to
implement custom functionality through a modular extension system.
