

.. index::
   pair: zsh; Shells
   pair: zsh-syntax-highlighting; zsh
   pair: zsh-autosuggestions; zsh
   pair: django; zsh

.. _zsh:

========================================
zsh (Z shell)
========================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Z_Shell
   - https://x.com/ohmyzsh
   - https://www.zsh.org/
   - https://sourceforge.net/p/zsh/code/ci/master/tree/FEATURES
   - https://sourceforge.net/p/zsh/code/ci/master/tree/NEWS
   - http://zsh.sourceforge.net/News/


.. contents::
   :depth: 4


Description, features
======================

.. seealso::

   - https://fr.wikipedia.org/wiki/Z_Shell
   - https://sourceforge.net/p/zsh/code/ci/master/tree/FEATURES


Le **Z shell** ou zsh est un shell Unix qui peut être utilisé de façon
interactive, à l'ouverture de la session ou en tant que puissant
interpréteur de commande.

Zsh peut être vu comme un **Bourne shell** étendu avec beaucoup d'améliorations.

Il reprend en plus la plupart des fonctions les plus pratiques de bash,
ksh et tcsh.



Installation
==============

.. toctree::
   :maxdepth: 3

   installation/installation


oh my zsh
===================

.. toctree::
   :maxdepth: 3

   oh_my_zsh/oh_my_zsh

Other Plugins
===============

.. toctree::
   :maxdepth: 3

   plugins/plugins


.zshrc examples
================

.. toctree::
   :maxdepth: 3

   zshrc/zshrc
