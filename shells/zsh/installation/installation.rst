

.. index::
   pair: zsh; installation

.. _zsh_installation:

==================
zsh installation
==================

.. seealso::

   - https://www.virtualmakerspace.com/ubuntu-setup-on-windows-oh-my-zsh-oh-my-vim/


.. contents::
   :depth: 3


Ubuntu installation
====================

::

    sudo apt install zsh
