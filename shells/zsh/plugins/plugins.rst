

.. index::
   pair: zsh; plugins
   pair: zsh; nvm
   pair: zsh ; Node Version Manager
   ! zsh plugins

.. _zsh_plugins:

=================
zsh plugins
=================

.. contents::
   :depth: 3

zsh-syntax-highlighting
=========================

.. seealso::

   - https://github.com/zsh-users/zsh-syntax-highlighting

zsh-autosuggestions
======================

.. seealso::

   - https://github.com/zsh-users/zsh-autosuggestions


.. _zsh_nvm:

zsh-nvm (Node Version Manager)
================================

.. seealso::

   - https://github.com/lukechilds/zsh-nvm
   - :ref:`nvm_nodejs`


Example::

    plugins=(django docker pip git git-extras git-flow-avh pyenv tmux z)

    # https://github.com/lukechilds/zsh-nvm
    plugins+=(zsh-nvm)

    source $ZSH/oh-my-zsh.sh
