

.. index::
   ! zshrc

.. _zsh_rc:

========================================
zshrc files
========================================

.. seealso::

   - https://github.com/GLMF/GLMF217/blob/master/Systeme_Reseau/zsh/zshrc


.. contents::
   :depth: 4


Example 1
===========

.. literalinclude:: example1/.zshrc
   :linenos:


Example 2
===========


.. literalinclude:: example1/.zshrc
   :linenos:
