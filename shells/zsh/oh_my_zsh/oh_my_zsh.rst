

.. index::
   pair: zsh; oh-my-zsh
   ! oh-my-zsh plugins

.. _oh_my_zsh:

========================================
oh-my-zsh
========================================

.. seealso::

   - https://x.com/ohmyzsh
   - https://ohmyz.sh/
   - https://github.com/robbyrussell/oh-my-zsh
   - https://github.com/robbyrussell/oh-my-zsh/wiki/Plugins


.. toctree::
   :maxdepth: 3

   plugins/plugins
   themes/themes
