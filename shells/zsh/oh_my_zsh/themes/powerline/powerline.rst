

.. index::
   pair: powerline; oh-my-zsh theme
   pair: powerline; fonts
   pair: powerline; Theme
   pair: fonts ; fc-cache -f -v
   pair: command ; fc-cache -f -v


.. _powerline_theme:

================================
Powerline theme with powerlevel
================================

.. contents::
   :depth: 3


Installation
==============


::

    git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k


::

	Clonage dans '/home/pvergain/.oh-my-zsh/custom/themes/powerlevel9k'...
	remote: Counting objects: 5752, done.
	remote: Compressing objects: 100% (4/4), done.
	remote: Total 5752 (delta 0), reused 2 (delta 0), pack-reused 5748
	Réception d'objets: 100% (5752/5752), 1.60 MiB | 888.00 KiB/s, fait.
	Résolution des deltas: 100% (3618/3618), fait.


Adding Powerline fonts
========================

.. seealso::

   - https://github.com/powerline/fonts

Ubuntu
--------

::

    sudo apt  install fonts-powerline

Centos7
---------

::

	# clone
	git clone https://github.com/powerline/fonts.git --depth=1
	# install
	cd fonts
	./install.sh
	# clean-up a bit
	cd ..
	rm -rf fonts


Actualiser le cache des polices du système
--------------------------------------------

.. seealso::

   - https://threadsec.wordpress.com/linux-zsh-theme-perso-agnoster/

::

    sudo fc-cache -f -v


ZSH_THEME="powerlevel9k/powerlevel9k"
=======================================


::

    ZSH_THEME="powerlevel9k/powerlevel9k"
