

.. index::
   pair: themes; oh-my-zsh

.. _oh_my_zsh_themes:

========================================
oh-my-zsh themes
========================================

.. seealso::

   - https://x.com/ohmyzsh


.. toctree::
   :maxdepth: 3

   powerline/powerline
