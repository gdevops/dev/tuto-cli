

.. index::
   pair: usage; oh-my-zsh

.. _oh_my_zsh_usage:

========================================
oh-my-zsh Usage
========================================

.. seealso::

   - https://github.com/robbyrussell/oh-my-zsh/wiki/Plugins


Enable the plugins you want by editing your ~/.zshrc file.
Example::

    plugins=(django docker git git-flow-avh pyenv tmux z)





