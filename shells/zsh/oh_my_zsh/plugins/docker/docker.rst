

.. index::
   pair: docker; oh-my-zsh

.. _oh_my_zsh_docker:

==================
oh-my-zsh docker
==================

.. seealso:: http://github.com/AeonAxan

Maintainer: Azaan

- Auto complete arguments and options for all docker commands.
- Show containerIDs and Images for tab completion where applicable (screenshots)

