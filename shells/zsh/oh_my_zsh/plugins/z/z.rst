

.. index::
   pair: z; oh-my-zsh

.. _oh_my_zsh_z:

========================================
oh-my-zsh z
========================================

Change directory based on history for example if you previously used::

    cd ~/.oh-my-zsh/plugins

Then using::

    z plug

Will move you to ~/.oh-my-zsh/plugins directory.





