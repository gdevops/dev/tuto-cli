

.. index::
   pair: pyenv; oh-my-zsh

.. _oh_my_zsh_pyenv:

=================
oh-my-zsh pyenv
=================

.. seealso::

   - https://github.com/ssm
   - https://github.com/pyenv/pyenv


Maintainer: ssm

- Completion for the pyenv command, which maintains local python installations.
- Exports a "pyenv_prompt_info()" function for your custom prompt.

