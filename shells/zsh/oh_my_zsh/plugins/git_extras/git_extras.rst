

.. index::
   pair: git-extras; oh-my-zsh

.. _zsh_git_extras:

==============
git-extras
==============

.. seealso::

  - :ref:`ref_git_extras`
