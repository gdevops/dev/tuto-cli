

.. index::
   pair: plugins; oh-my-zsh

.. _oh_my_zsh_plugins:

========================================
oh-my-zsh plugins
========================================

.. seealso::

   - https://x.com/ohmyzsh
   - https://ohmyz.sh/
   - https://github.com/robbyrussell/oh-my-zsh
   - https://github.com/robbyrussell/oh-my-zsh/wiki/Plugins

.. toctree::
   :maxdepth: 3

   usage/usage
   django/django
   docker/docker
   git_extras/git_extras
   git_flow_avh/git_flow_avh
   pyenv/pyenv
   tmux/tmux
   z/z
