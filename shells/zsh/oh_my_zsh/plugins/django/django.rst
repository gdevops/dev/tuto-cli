

.. index::
   pair: django; oh-my-zsh


.. _oh_my_zsh_django:

==================
oh-my-zsh django
==================

.. seealso::

   - https://github.com/robbyrussell
   - https://github.com/robbyrussell/oh-my-zsh/wiki/Plugins#django


Maintainer: robbyrussell

Adds completion for django and manage.py. `See for more info`_.

.. _`See for more info`:  https://github.com/robbyrussell/oh-my-zsh/wiki/Plugins#django

