

.. index::
   pair: tmux; oh-my-zsh

.. _oh_my_zsh_tmux:

========================================
oh-my-zsh tmux
========================================

.. seealso::

   - https://github.com/tmux/tmux/wiki
   - https://github.com/robbyrussell/oh-my-zsh/wiki/Plugins#tmux


Adds several options for effecting the startup behavior of tmux.
Each of the options are set by changing the environment variables
below in the TOP of your ~/.zshrc file:

- ZSH_TMUX_AUTOSTART: Automatically start a tmux session upon logging in.
  Set to false by default.
- ZSH_TMUX_AUTOSTART_ONCE: Only attempt to autostart tmux once.
  If this is disabled when the previous option is enabled, then tmux
  will be autostarted every time you source your zsh config files.
  Set to true by default.
- ZSH_TMUX_AUTOCONNECT: When running tmux automatically connect to the
  currently running tmux session if it exists, otherwise start a
  new session. Set to true by default.
- ZSH_TMUX_AUTOQUIT: Close the terminal session when tmux exits.
  Set to the value of ZSH_TMUX_AUTOSTART by default.
- ZSH_TMUX_FIXTERM: When running tmux, the variable $TERM is supposed
  to be set to screen or one of its derivatives.
  This option will set the default-terminal option of tmux to
  screen-256color if 256 color terminal support is detected, and screen
  otherwise. The term values it uses can be overridden by changing the
  ZSH_TMUX_FIXTERM_WITH_256COLOR and ZSH_TMUX_FIXTERM_WITHOUT_256COLOR
  variables respectively. Set to true by default.

If the user specifies any arguments for tmux then the command is just executed as it was typed it without any modification.





