.. index::
   pair: bash; Shells

.. _bash:

========================================
**GNU bash** (Bourne-again shell)
========================================

.. seealso::

   - https://en.wikipedia.org/wiki/Bash_%28Unix_shell%29
   - https://git.savannah.gnu.org/cgit/bash.git


.. figure:: Gnu_bash_logo_svg.png
   :align: center


.. contents::
   :depth: 4


Description, features
======================

**GNU Bash** or simply Bash is a Unix shell and command language written by
Brian Fox for the GNU Project as a free software replacement for the
Bourne shell.

First released in 1989 it has been used widely as the default login shell
for most Linux distributions and Apple's macOS Mojave and earlier versions.

A version is also available for Windows 10. It is also the default user
shell in Solaris 11.

Bash is a command processor that typically runs in a text window where
the user types commands that cause actions.
Bash can also read and execute commands from a file, called a shell script.

Like all Unix shells, it supports filename globbing (wildcard matching),
piping, here documents, command substitution, variables, and control
structures for condition-testing and iteration.

The keywords, syntax, dynamically scoped variables and other basic
features of the language are all copied from sh. Other features, e.g.,
history, are copied from csh and ksh.

Bash is a POSIX-compliant shell, but with a number of extensions.

The shell's name is an acronym for Bourne-again shell, a pun on the name
of the Bourne shell that it replaces and the notion of being "born again".


Définition en français
=========================

**Bash** (acronyme de Bourne-Again shell) est un interpréteur en ligne de
commande de type script. C'est le shell Unix du projet GNU.

Fondé sur le Bourne shell, Bash lui apporte de nombreuses améliorations,
provenant notamment du Korn shell et du C shell.

Bash est un logiciel libre publié sous licence publique générale GNU.

Il est l'interprète par défaut sur de nombreux Unix libres, notamment
sur les systèmes GNU/Linux.

C'était aussi le shell par défaut de Mac OS X, remplacé avec Mac OS
Catalina (v10.15) par zsh.

Il a été d'abord porté sous Microsoft Windows par le projet Cygwin, et
depuis Windows 10 constitue une option à part entière, nommée
Windows Subsystem for Linux du système d'exploitation.



Quotes in bash
==============

.. seealso::

   - https://wizardzines.com/comics/bash-quotes/

.. figure:: quotes_bash.jpeg
   :align: center

   https://x.com/b0rk/status/1313570733071781898?s=20



Scripts exécutés lors du lancement de bash
=============================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Bourne-Again_shell#Scripts_ex%C3%A9cut%C3%A9s_lors_du_lancement_de_bash


Scripts exécutés quand bash est utilisé pour une ouverture de session
--------------------------------------------------------------------------

communs à tous les utilisateurs:

- /etc/profile

spécifiques à chaque utilisateur:

- .bash_profile
- .bash_login
- .profile
- .bash_logout : Script exécuté lors de la déconnexion

Scripts exécutés quand bash n'est pas utilisé pour une ouverture de session
-----------------------------------------------------------------------------

spécifiques à chaque utilisateur:

- .bashrc.


powerline.bash
===============

.. seealso::

   - :ref:`powerline_bash`
