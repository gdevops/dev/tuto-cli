.. index::
   pair: CLI; Shells

.. _shells:

========================================
Shells
========================================


.. toctree::
   :maxdepth: 5

   bash/bash
   nu_shell/nu_shell
   zsh/zsh
