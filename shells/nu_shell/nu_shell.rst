
.. index::
   pair: nushell; Shells
   ! nushell

.. _nushell:

========================================================
**nushell A new type of shell written in @rustlang**
========================================================


- https://x.com/nu_shell
- https://github.com/nushell/nushell
- https://www.nushell.sh/
- https://www.nushell.sh/book/
- https://www.nushell.sh/blog/
