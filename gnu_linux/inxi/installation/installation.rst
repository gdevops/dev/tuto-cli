

.. _inxi_install:

========================================
inxi installation
========================================


.. contents::
   :depth: 3


git clone
===========
::

    git clone https://github.com/smxi/inxi --branch inxi-perl --single-branch


::

    Clonage dans 'inxi'...
    remote: Enumerating objects: 134, done.
    remote: Counting objects: 100% (134/134), done.
    remote: Compressing objects: 100% (87/87), done.
    remote: Total 8212 (delta 94), reused 86 (delta 47), pack-reused 8078
    Réception d'objets: 100% (8212/8212), 4.95 MiB | 1.30 MiB/s, fait.
    Résolution des deltas: 100% (5292/5292), fait.


::

    cd inxi

::

    ~/inxi > ls -als

::

    total 760
    4 drwxr-xr-x  6    4096 avril 19 12:56 .
    4 drwxr-xr-x 65    4096 avril 19 12:56 ..
    4 drwxr-xr-x  2    4096 avril 19 12:56 docs
    4 drwxr-xr-x  8    4096 avril 19 12:56 .git
    4 drwxr-xr-x  2    4096 avril 19 12:56 lib
    36 -rw-r--r--  1   35141 avril 19 12:56 LICENSE.txt
    4 drwxr-xr-x  2    4096 avril 19 12:56 modules
    636 -rwxr-xr-x  1  650015 avril 19 12:56 pinxi
    56 -rw-r--r--  1   53733 avril 19 12:56 pinxi.1
    8 -rw-r--r--  1    6881 avril 19 12:56 README.txt


::

    tree -L 3

    .
    ├── docs
    │   ├── cpu-flags.txt
    │   ├── inxi-data.txt
    │   ├── inxi-fragments.txt
    │   ├── inxi-resources.txt
    │   ├── inxi-tools.txt
    │   ├── inxi-values.txt
    │   ├── optimization.txt
    │   ├── perl-programming.txt
    │   ├── perl-setup.txt
    │   ├── perl-version-support.txt
    │   └── usb-ids.txt
    ├── lib
    │   ├── colors_configs.pl
    │   ├── compiler.pl
    │   ├── desktop.pl
    │   ├── distro.pl
    │   ├── downloaders.pl
    │   ├── error_handler.pl
    │   ├── file_downloader_fetch.pl
    │   ├── file_uploader.pl
    │   ├── init.pl
    │   ├── ls_dir.pl
    │   ├── memory.pl
    │   ├── options.pl
    │   ├── print_test.pl
    │   ├── processes.pl
    │   ├── README.txt
    │   ├── repos.pl
    │   ├── show_options.pl
    │   ├── start_client.pl
    │   ├── system_debugger.pl
    │   ├── sys_tree_ls.pl
    │   ├── sys_tree_traverse.pl
    │   ├── template.pl
    │   ├── tester
    │   ├── test.pl
    │   ├── updater.pl
    │   └── weather.pl
    ├── LICENSE.txt
    ├── modules
    │   └── README.txt
    ├── pinxi
    ├── pinxi.1
    └── README.txt


Update
======

::

    cd ~/inxi
    git pull
