

.. index::
   pair: GNU/Linux; inxi
   ! inxi

.. _inxi:

========================================
inxi
========================================

- https://smxi.org/docs/inxi.htm
- https://github.com/smxi/inxi/blob/master/inxi
- https://command-not-found.com/inxi

.. toctree::
   :maxdepth: 3

   requirements/requirements
   installation/installation
   examples/examples
   versions/versions
