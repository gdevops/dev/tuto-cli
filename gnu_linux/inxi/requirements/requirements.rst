

.. index::
   pair: requirements; inxi

.. _inxi_requirements:

========================================
inxi requirements
========================================

.. contents::
   :depth: 3

Debian requirements
====================
::

    sudo apt show source inxi

::

    Package: inxi
    Version: 3.0.27-1-1~18.04
    Priority: optional
    Section: misc
    Maintainer: Unit 193 <unit193@ubuntu.com>
    Installed-Size: 759 kB
    Depends: pciutils, procps
    Recommends: dmidecode, dnsutils, file, hddtemp, iproute2 | net-tools, kmod, lm-sensors, mesa-utils, sudo, tree, usbutils, x11-utils, x11-xserver-utils
    Suggests: libcpanel-json-xs-perl | libjson-xs-perl, libxml-dumper-perl, wget | curl | libhttp-tiny-perl
    Homepage: https://smxi.org/docs/inxi.htm
    Download-Size: 254 kB
    APT-Manual-Installed: yes
    APT-Sources: http://packages.linuxmint.com tessa/import amd64 Packages

    Description: full featured system information script
    Inxi is a system information script that can display various things about
    your hardware and software to users in an IRC chatroom or support forum.
    It runs with the /exec command in most IRC clients.
