

.. _inxi_examples:

========================================
inxi examples
========================================

.. contents::
   :depth: 3

inxi -Fx
==========

::

    inxi -Fx

::

    System:    Host: UC004 Kernel: 4.15.0-47-generic x86_64 bits: 64 compiler: gcc v: 7.3.0 Desktop: Cinnamon 4.0.10
               Distro: Linux Mint 19.1 Tessa base: Ubuntu 18.04 bionic
    Machine:   Type: Desktop System: Hewlett-Packard product: HP Elite 7500 Series MT v: 1.00 serial: <root required>
               Mobo: PEGATRON model: 2AD5 v: 1.03 serial: <root required> BIOS: AMI v: 8.21 date: 10/20/2014
    CPU:       Topology: Quad Core model: Intel Core i5-3470 bits: 64 type: MCP arch: Ivy Bridge rev: 9 L2 cache: 6144 KiB
               flags: lm nx pae sse sse2 sse3 sse4_1 sse4_2 ssse3 vmx bogomips: 25542
               Speed: 3553 MHz min/max: 1600/3600 MHz Core speeds (MHz): 1: 3539 2: 3592 3: 3592 4: 3590
    Graphics:  Device-1: Intel Xeon E3-1200 v2/3rd Gen Core processor Graphics vendor: Hewlett-Packard driver: i915 v: kernel
               bus ID: 00:02.0
               Display: x11 server: X.Org 1.19.6 driver: modesetting unloaded: fbdev,vesa
               resolution: 1920x1080~60Hz, 1920x1080~60Hz
               OpenGL: renderer: Mesa DRI Intel Ivybridge Desktop v: 4.2 Mesa 18.2.8 direct render: Yes
    Audio:     Device-1: Intel 7 Series/C216 Family High Definition Audio vendor: Hewlett-Packard driver: snd_hda_intel v: kernel
               bus ID: 00:1b.0
               Sound Server: ALSA v: k4.15.0-47-generic
    Network:   Device-1: Qualcomm Atheros AR8161 Gigabit Ethernet vendor: Hewlett-Packard driver: alx v: kernel port: e000
               bus ID: 02:00.0
               IF: eno1 state: up speed: 1000 Mbps duplex: full mac: 70:54:d2:96:ad:e6
               IF-ID-1: br-21b09f174136 state: down mac: 02:42:9f:e1:e7:14
               IF-ID-2: br-f99838229084 state: down mac: 02:42:f4:48:b3:c7
               IF-ID-3: docker0 state: down mac: 02:42:c6:e6:37:41
    Drives:    Local Storage: total: 931.51 GiB used: 142.56 GiB (15.3%)
               ID-1: /dev/sda vendor: Seagate model: ST1000DM003-1CH162 size: 931.51 GiB
    Partition: ID-1: / size: 907.95 GiB used: 142.56 GiB (15.7%) fs: ext4 dev: /dev/dm-0
               ID-2: swap-1 size: 7.87 GiB used: 12 KiB (0.0%) fs: swap dev: /dev/dm-1
    Sensors:   System Temperatures: cpu: 29.8 C mobo: 27.8 C
               Fan Speeds (RPM): N/A
    Info:      Processes: 245 Uptime: 2h 09m Memory: 7.66 GiB used: 3.21 GiB (41.9%) Init: systemd runlevel: 5 Compilers:
               gcc: 7.3.0 Shell: zsh v: 5.4.2 inxi: 3.0.27
