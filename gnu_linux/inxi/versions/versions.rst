

.. index::
   pair: Versions; inxi

.. _inxi_versions:

========================================
inxi versions
========================================

.. seealso::

   - https://github.com/smxi/inxi/blob/3.0.33-1/inxi.changelog
   - https://github.com/smxi/inxi/blob/master/inxi
   - https://github.com/smxi/inxi/releases

.. toctree::
   :maxdepth: 3

   3.0.33/3.0.33
