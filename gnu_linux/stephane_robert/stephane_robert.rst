.. index::
   pair: Stéphane Robert; DevOps
   ! Stéphane Robert

.. _devops_gnu_linux_commands:

======================================================================================
DevOps - Ma liste d'outils en ligne de commande indispensables par Stéphane Robert
======================================================================================

- https://blog.stephane-robert.info/post/devops-outils-linux-indispensables/

Je vous propose une sélection d'outils DevOps que je considère indispensables
dans le domaine de l'administration système et de la gestion de projet
informatique DevOps.

Cette sélection vous aidera à améliorer votre productivité et à
simplifier la gestion de vos projets DevOps. Vous y trouverez des outils pour
de l'administration systèmes à la gestion de version avec Git, en passant par
la conteneurisation, à l'automatisation des déploiements avec Gitlab CI/CD ou
Github Actions, la gestion de configuration avec Ansible, du Provisionnement
avec Terraform et la sécurité avec OpenScap, Trivy ... Chaque outil sera
ensuite détaillé dans un guide dédié.
