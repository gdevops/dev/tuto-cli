﻿
.. _find_strings:

==========================================
**find strings in files recursively**
==========================================

.. toctree::
   :maxdepth: 3

   find/find
   grep/grep
   ripgrep/ripgrep
