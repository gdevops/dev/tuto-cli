﻿.. index::
   ! grep


.. _grep_command:

============================
With the **grep** command
============================

.. code-block:: bash

    grep  -R 'saisie_conges.html' --include="*.py"
