﻿#!/usr/bin/env bash
# Extract sound

mkdir mp3

for filename in *.mp4; do
    input=${filename}
    name=${filename%.*}
    suffixe="mp3"
    output="mp3/"${name}.${suffixe}
    echo "Input: ${input}"
    echo "File Name: ${name}"
    echo "File Extension: ${suffixe}"
    echo "output: ${output}"

    ffmpeg -i ${input}   -vn -acodec libmp3lame -q:a 4  ${output}
done

