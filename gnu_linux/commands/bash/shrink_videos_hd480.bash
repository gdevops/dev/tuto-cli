﻿#!/usr/bin/env bash
#
# The HD definition will be 'hd480'
#
# https://shrink-my-video.onrender.com/shrink-my-video/
# https://gdevops.frama.io/dev/tuto-cli/gnu_linux/commands/shrink_videos.html
#
hd_definition=hd480
mkdir ${hd_definition}

for filename in *.mp4; do
    input=${filename}
    name=${filename%.*}
    suffixe=${filename##*.}
    output="hd480/"${name}_${hd_definition}.${suffixe}
    echo "Input: ${input}"
    echo "File Name: ${name}"
    echo "File Extension: ${suffixe}"
    echo "output: ${output}"

    ffmpeg -i ${input} -c:v libx264 -s ${hd_definition} -crf 22 -c:a aac -b:a 160k -vf "scale=iw*sar:ih,setsar=1" ${output}

done
