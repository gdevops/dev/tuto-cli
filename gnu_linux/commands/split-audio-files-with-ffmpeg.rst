﻿
.. index::
   pair: Split; Audio (ffmpeg)


.. _split_audio_images:

==========================================
Split an audio file in multiple parts
==========================================

- https://www.veed.io/tools/audio-to-text


::

    ffmpeg -i akadem.wav  -f segment -segment_time 300 -c copy "akadem_%03d.wav"
