﻿.. index::
   pair : cli ; (extract sound)
   pair : ffmpeg ; ffmpeg -i input.mp4   -vn -acodec libmp3lame -q:a 4 output.mp3 (extract sound)
   pair : sound ; mp3
   pair: sound ; extract sound with ffmpeg
   pair: ffmpeg ; extract sound with ffmpeg


.. _extract_sound_from_a_video_with_ffmpeg:

==============================================================
**Extract sound from mp4 and produces a mp3** (-with ffmpeg)
==============================================================


Firefox extension : easy-youtube-video-download
=====================================================

- https://addons.mozilla.org/fr/firefox/addon/easy-youtube-video-download/

Extract sound from flv and produces a mp3
===============================================

Source: Linux Pratique numéro 63, p.23

Vous venez de récupérer une vidéo depuis un site de publications du type Youtube
ou DailyMotion, dont vous aimeriez bien récupérer la bande son ?

C'est idéal dans le cas de clip vidéo d'un artiste que vous appréciez par exemple...

Dans ce cas, la commande suivante, utilisant ffmpeg vous sera très utile.



::

    ffmpeg -i input.flv -ar 44100 -ab 192k -ac 2 output.mp3


L'option ``-i``
    permet de définir le fichier vidéo en entrée.

L'option ``-ar``
    définit la fréquence d'échantillonnage (par défaut 44100Hz)

L'option ``-ac``
    permet de définir le nombre de canaux audio

L'option ``_ab``
    désigne le bitrate audio (64k par défaut)


Extract sound from mp4 and produces a mp3 (ffmpeg -i input.mp4   -vn -acodec libmp3lame -q:a 4 output.mp3)
==================================================================================================================


::

    ffmpeg -i input.mp4   -vn -acodec libmp3lame -q:a 4 output.mp3


Script d'extraction de fichiers son à partir de mp4 avec ffmpeg
========================================================================

.. literalinclude:: bash/extract_sound.bash
