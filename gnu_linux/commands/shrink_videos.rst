﻿
.. index::
   pair: Resize; Videos
   pair: Shrink ; videos
   pair: ffmpeg ; how to shrink a list of mp4 files with ffmpeg


.. _resizing_videos:
.. _shrink_videos:

==================================================================================================
**Shrink video** 🌳 **Upload your video then get a smaller one, easy!** (Thanks to ffmpeg)
==================================================================================================

- https://shrink-my-video.onrender.com/shrink-my-video/

::

    ffmpeg -i input.mp4 -c:v libx264 -s hd480 -crf 22 -c:a aac -b:a 160k -vf "scale=iw*sar:ih,setsar=1" output.mp4


**Script bash how to shrink a list of mp4 files with ffmpeg**
=================================================================

.. literalinclude:: bash/shrink_videos_hd480.bash
   :language: bash
   :linenos:
