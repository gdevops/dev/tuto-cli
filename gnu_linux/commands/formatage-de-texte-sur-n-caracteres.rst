
.. index::
   pair: fmt; Formatage de texte

===========================================
**Formatage de texte sur n caractères**
===========================================

Le texte à formater est dans le fichier t.txt.
Le texte formaté est dans ourput.rst

::

    fmt -w 82 < t.txt > output.rst
