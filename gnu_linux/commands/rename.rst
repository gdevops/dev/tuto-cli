﻿.. index::
   pair: CLI ; renaming files

.. _renaming_files:

==============
Renaming files
==============

.. contents::
   :depth: 3


rename installation
====================

::

    sudo apt install rename

::

    Lecture des listes de paquets... Fait
    Construction de l'arbre des dépendances
    Lecture des informations d'état... Fait
    Les NOUVEAUX paquets suivants seront installés :
      rename
    0 mis à jour, 1 nouvellement installés, 0 à enlever et 222 non mis à jour.
    Il est nécessaire de prendre 17,2 ko dans les archives.
    Après cette opération, 50,2 ko d'espace disque supplémentaires seront utilisés.
    Réception de :1 http://deb.debian.org/debian buster/main amd64 rename all 1.10-1 [17,2 kB]
    17,2 ko réceptionnés en 0s (91,7 ko/s)
    Sélection du paquet rename précédemment désélectionné.
    (Lecture de la base de données... 222992 fichiers et répertoires déjà installés.)
    Préparation du dépaquetage de .../archives/rename_1.10-1_all.deb ...
    Dépaquetage de rename (1.10-1) ...
    Paramétrage de rename (1.10-1) ...
    update-alternatives: utilisation de « /usr/bin/file-rename » pour fournir « /usr/bin/rename » (rename) en mode automatique


rename command
==============

.. seealso:: http://www.commandlinefu.com/commands/tagged/404/rename

Rename files from .c to .cpp
------------------------------

.. code-block:: bash

    rename 's/\.c/\.cpp/' *.c

Rename files from .MOD to .MPG
------------------------------

.. code-block:: bash

    rename 's/\.MOD/\.MPG/' *.MOD


Rename files from .txt to .rst
------------------------------

.. code-block:: bash

   rename 's/\.txt/\.rst/' *.txt
