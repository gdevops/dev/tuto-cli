﻿

==================================================================
**Rechercher les gros fichiers et les plus gros répertoires**
==================================================================

Trouver les fichiers de plus de 400Mo
====================================================

.. code-block:: bash

    find . -type f -size +400M

Trouver les plus gros répertoires
====================================

::

    du -h --max-depth=1 2> /dev/null | sort -hr | tail -n +2 | head


::

    53G	./.cache
    17G	./.mozilla
    7,4G	./.config
    5,6G	./.local
    5,1G	./.thunderbird
    4,9G	./Documents
    4,3G	./projets
    3,0G	./.cargo
    2,6G	./groupes_informatique_id3
