﻿.. index::
   pair: find ;count the number of lines

.. _find_count_number_of_lines:

============================
**find** command
============================

Display files, the number of files and the count the number of lines
========================================================================


.. code-block:: bash

    find . -name "*.md"
    find . -name "*.py" -type f -exec wc -l "{}" \+
    find . -name "*.sh" | wc -l
    find . -name "*.sh" -type f -exec wc -l "{}" \+
    find . -name "*.html" | wc -l
    find . -name "*.html" -type f -exec wc -l "{}" \+
    find . -name "*.css" | wc -l
    find . -name "*.css" -type f -exec wc -l "{}" \+
    find . -name "*.js" | wc -l
    find . -name "*.js" -type f -exec wc -l "{}" \+
    find . -name "*.json" -type f -exec wc -l "{}" \+
    find . -name "*.xml" | wc -l
    find . -name "*.xml" -type f -exec wc -l "{}" \+
    find . -name "*.png" | wc -l
    find . -name "*.png"
    find . -name "*.jpg" | wc -l
    find . -name "*.jpg"
