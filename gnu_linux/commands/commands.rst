﻿

.. index::
   pair: Unix shell ; cli


.. _unix_shell_commands:

======================
Unix shell commands
======================

- https://www.commandlinefu.com/commands/browse


.. toctree::
   :maxdepth: 4

   find/find
   count_files_h
   count_lines_h
   crop_images
   delete/delete
   formatage-de-texte-sur-n-caracteres
   split-audio-files-with-ffmpeg
   delete_some_data_in_files
   find_strings/find_strings
   find_big_files
   rename
   replace_strings/replace_strings
   bar
   groups
   resize_images
   extract_sound_from_video
   write_in_file
   shrink_videos
