﻿.. index::
   pair: Resize; Images
   pair: Resize Image; convert
   pair: Resize Image; mogrify
   pair: webp; convert
   pair: script bash; how to shrink a list of jpg files with the 'convert' command


.. _resizing_image_sizes:

=========================================================
Resizing image files and eventually convert to webp
=========================================================

- http://www.commandlinefu.com/commands/tagged/1066/resize

mogrify command
===============


Resize all JPEGs in a directory

This command requires the imagemagick libraries and will
resize all files with the .JPG extension to a width of 800 pixels and will
keep the same proportions as the original image.

::

    mogrify -resize 800 *.JPG




**Script bash : how to shrink a list of jpg files with the 'convert' command**
===================================================================================

resize only
-------------------

.. literalinclude:: bash/resize_images_800.bash
   :language: bash
   :linenos:

resize and convert to webp
-------------------------------

.. literalinclude:: bash/resize_images_800_webp.bash
   :language: bash
   :linenos:


In the past (2011) with google picasa use case
========================================================

::

    cp *.JPG web
    cd web
    mogrify -resize 800 *.JPG
    date; google picasa -d "2011-10-27" -n "Minou et Ali à Annecy le 27 octobre 2011" -t "Annecy, 27 octobre 2011" create "Annecy, 27 octobre 2011" *.JPG; date
