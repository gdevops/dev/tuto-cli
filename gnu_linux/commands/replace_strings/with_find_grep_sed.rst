﻿.. index::
   pair: Find and replace; grep and sed

.. _replace_strings_find_grep_sed:

=================================================
**Search and replace with find grep and sed**
=================================================


.. code-block:: bash

    find . -name "*.rst" -print0 | xargs -0 sed -i 's/1.1.1/1.2.0/g'
    find . -name "conf.py" -print0 | xargs -0 sed -i 's/https:\/\/cnt-f.gitlab.io/https:\/\/gassr.gitlab.io/g'
    find . -name "conf.py" -print0 | xargs -0 sed -i 's/gitlab.io\/tuto_python/gitlab.io\/python\/tuto/g'
    find . -name "*.rst" -print0 | xargs -0 sed -i 's/fedi.com/x.com/g'
