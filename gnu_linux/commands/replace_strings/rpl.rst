﻿
.. index::
   pair: Replace strings ; rpl
   ! rpl


.. _replace_strings_rpl:

============================================
recursive replace string in files with rpl
============================================


- http://www.commandlinefu.com/commands/matching/replace-string-in-files-recursively/cmVwbGFjZSBzdHJpbmcgaW4gZmlsZXMgcmVjdXJzaXZlbHk=/sort-by-votes

Dry run 
====================


.. code-block:: bash

    rpl -Rs -x'.rst' '2010-2012' '2010-2013' .
    rpl -Rs olstring  newstring  .
    rpl -R --dry-run -x'*.py' "CATEGORIE_TRANSVERSE" "CATEGORIE_PRINCIPALE" .



Replace
=============================

.. code-block:: bash

    rpl -R -x'.rst' '2010-2012' '2010-2013' .
    rpl -R olstring  newstring  .
    rpl -R -x'*.py' "CATEGORIE_TRANSVERSE" "CATEGORIE_PRINCIPALE" .
