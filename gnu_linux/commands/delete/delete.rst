﻿.. index::
   pair: delete ; _build directories

.. _delete:

============================
**delete** command
============================

Delete files or directories
========================================================================


.. code-block:: bash

    find . -type d -name '_build' | xargs rm -r  # supression des répertoires _build (exemple pour sphinx)
