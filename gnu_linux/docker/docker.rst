

.. index::
   pair: docker ; Suppression des images docker inutiles

.. _docker:

========================================
docker
========================================



Delete all stopped containers, all networks not used by at least one container, all dangling images, unused build cache
=============================================================================================================================

WARNING! This will remove:

- all stopped containers
- all networks not used by at least one container
- all dangling images
- unused build cache

::

    docker system prune

Supprimer les images docker inutiles
================================================================

::

    docker rmi $(docker images --filter "dangling=true" -q)
