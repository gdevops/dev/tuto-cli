

.. index::
   pair: CLI; Python

.. _python_cli:

========================================
Python
========================================


.. toctree::
   :maxdepth: 5

   arguments_parsers/argument_parsers
   delegator/delegator
