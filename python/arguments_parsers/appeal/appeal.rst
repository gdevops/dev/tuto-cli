
.. index::
   pair: Arguments parser; appeal
   ! appeal

.. _appeal:

=============================================================
**appeal: Command-line parsing library for Python 3**
=============================================================


- https://github.com/larryhastings/appeal


Overview
=============

Appeal is a command-line argument processing library for Python, like
argparse, optparse, getopt, click, docopt, and Typer.

But Appeal takes a refreshing new approach.

Other libraries have complicated, cumbersome interfaces that force you
to repeat yourself over and over.

Appeal leverages Python's rich function call interface, **which makes
defining your command-line interface effortless**.
You write Python functions, and Appeal translates them into command-line
options and arguments.

Appeal provides amazing power and flexibility--but it's also intuitive,
because it mirrors Python itself.
If you understand how to write Python functions, you're already halfway
to understanding Appeal!

A New And Appealing Approach
==============================

Appeal isn't like other command-line parsing libraries.

In fact, you really shouldn't think of Appeal as a "command-line parsing library"
per se.

And, although you work with Appeal by passing in functions for Appeal
to call, you shouldn't think of these functions as "callbacks".

**Appeal lets you design APIs callable from the command-line**.

It's just like any other Python library API--except that the caller
calls you from the command-line instead of from Python.

Appeal is the mechanism converting between these two domains:

- it translates your API into command-line semantics,
- then translates the user's command-line back into calls to your API.

This raises another good point: the API you build using Appeal also
often makes for a very nice automation API, allowing your program to also
be used as a library by other programs with minimal effort.
