

.. _delegator_installation:

==========================================
delegator.py installation
==========================================

.. seealso::

   - https://github.com/kennethreitz/delegator.py


::

    $ pip install delegator.py


::

    pip install delegator.py
    Collecting delegator.py
    Downloading https://files.pythonhosted.org/packages/a7/c2/2860c52ef858c4672b6cf637f473e9139342cdb281135db9b3c32cfb0a85/delegator.py-0.1.1-py2.py3-none-any.whl
    Collecting pexpect>=4.1.0 (from delegator.py)
    Downloading https://files.pythonhosted.org/packages/0e/3e/377007e3f36ec42f1b84ec322ee12141a9e10d808312e5738f52f80a232c/pexpect-4.7.0-py2.py3-none-any.whl (58kB)
    100% |████████████████████████████████| 61kB 1.2MB/s
    Collecting ptyprocess>=0.5 (from pexpect>=4.1.0->delegator.py)
    Downloading https://files.pythonhosted.org/packages/d1/29/605c2cc68a9992d18dada28206eeada56ea4bd07a239669da41674648b6f/ptyprocess-0.6.0-py2.py3-none-any.whl
    Installing collected packages: ptyprocess, pexpect, delegator.py
    Successfully installed delegator.py-0.1.1 pexpect-4.7.0 ptyprocess-0.6.0
