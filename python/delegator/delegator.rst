

.. index::
   pair: delegator; Python
   ! delegator

.. _cli_python_delegator:

=================================================
**delegator.py** (Subprocesses for Humans 2.0.)
=================================================


- https://github.com/kennethreitz/delegator.py


.. toctree::
   :maxdepth: 3

   definition/definition
   installation/installation
   usage/usage
