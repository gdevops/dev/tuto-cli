

.. index::
   pair: delegator; Usage

.. _delegator_usage_1:

==========================================
delegator.py usage
==========================================


- https://github.com/kennethreitz/delegator.py


tasks.py
=========

.. code-block:: python
   :linenos:

    """tasks.py


    """

    from invoke import task
    import delegator


    @task
    def rmimages(c):
        """Deleting dangling docker images

            docker rmi $(docker images --filter "dangling=true" -q)
        """
        commande = delegator.run("docker images --filter 'dangling=true' -q")
        liste_delete = commande.out.replace('\n', " ")
        commande = delegator.run(f"docker rmi {liste_delete}")
        print(f"{commande} {commande.out}")
        commande = delegator.chain('fortune | cowsay')
        print(f"{commande} {commande.out}")


Calling with invoke
====================

::

    invoke --list

::

    Available tasks:

      rmimages   Deleting dangling docker images



::

    <Command 'docker rmi '>
    <Command ['cowsay']>  _______________________________________
    / Certainly the game is rigged.         \
    |                                       |
    | Don't let that stop you; if you don't |
    | bet, you can't win.                   |
    |                                       |
    | -- Robert Heinlein, "Time Enough For  |
    \ Love"                                 /
     ---------------------------------------
            \   ^__^
             \  (oo)\_______
                (__)\       )\/\
                    ||----w |
                    ||     ||
