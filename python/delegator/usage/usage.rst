

.. index::
   pair: delegator; Usage

.. _delegator_usage:

==========================================
delegator.py usage
==========================================

.. seealso::

   - https://github.com/kennethreitz/delegator.py


.. toctree::
   :maxdepth: 3

   invoke_1/invoke_1
