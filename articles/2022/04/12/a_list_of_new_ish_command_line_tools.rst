.. index::
   pair: Julia Evans; 2022-04-12

.. _julia_evans_2022_04_12:

=================================================================
2022-04-12 A list of new(ish) command line tools by Julia Evans
=================================================================

- https://jvns.ca/blog/2022/04/12/a-list-of-new-ish--command-line-tools/
- https://github.com/ibraheemdev/modern-unix


Introduction
=============

Hello! Today I asked on twitter about newer command line tools, like
ripgrep and fd and fzf and exa and bat.

I got a bunch of replies with tools I hadn’t heard of, so I thought I’d
make a list here. A lot of people also pointed at the `modern-unix list <https://github.com/ibraheemdev/modern-unix>`_.


replacements for standard tools
==================================

::

    ripgrep, ag, ack (grep)
    exa, lsd (ls)
    mosh (ssh)
    bat (cat)
    delta (a pager for git)
    fd (find)
    drill, dog (dig)
    duf (df)
    dust, ncdu (du)
    pgcli (psql)
    btm, btop, glances, gtop, zenith (top)
    tldr (man, sort of)
    sd (sed)
    difftastic (diff)
    mtr (traceroute)
    plocate (locate)
    xxd, hexyl (hexdump)

- `duf <https://github.com/muesli/duf>`_ (df)

new inventions
==================

Here are some tools that are not exactly replacements for standard tools

::

    z, fasd, autojump, zoxide (tools to make it easier to find files / change directories)
    broot, nnn, ranger (file manager)
    direnv (load environment variables depending on the current directory)
    fzf, peco (“fuzzy finder”)
    asdf (version manager for multiple languages)
    tig, lazygit (interactive interfaces for git)
    lazydocker (interactive interface for docker)
    choose (the basics of awk/cut)
    ctop (top for containers)
    fuck (autocorrect command line errors)
    tmate (share your terminal with a friend)
    lnav, angle-grinder (tools for managing logs)
    mdp, glow (ways to display markdown in the terminal)
    pbcopy/pbpaste (for clipboard <> stdin/stdout) maybe aren’t “new” but were mentioned a lot. You can use xclip to do the same thing on Linux.



- `hyperfine <https://github.com/sharkdp/hyperfine>`_ (benchmarking)
- `entr <https://github.com/eradman/entr>`_ (run arbitrary commands when files change)
- `croc <https://github.com/schollz/croc>`_ and `magic-wormhole <https://github.com/magic-wormhole/magic-wormhole>`_ (send files from one computer to another)
-  `httpie <https://httpie.io/>`_, `curlie <https://github.com/rs/curlie>`_, `xh <https://github.com/ducaale/xh>`_ (for making HTTP requests)


JSON/YAML/CSV things
======================

::

    jq (a great JSON-wrangling tool)
    jc (convert various tools’ output into JSON)
    jo (create JSON objects)
    yq (like jq, but for YAML). there’s also another yq
    fq (like jq, but for binary)
    fx (interactive json tool)
    jless (json pager)
    xsv (a command line tool for csv files, from burntsushi)
    visidata (“an interactive multitool for tabular data”)
    miller (“like awk/sed/cut/join/sort for CSV/TSV/JSON/JSON lines”)


- `htmlq <https://github.com/mgdm/htmlq>`_ (like jq, but for HTML)

grep things
=============

::

    pdfgrep (grep for PDF)
    gron (make JSON greppable)
    ripgrep-all (ripgrep, but also PDF, zip, ebooks, etc)

less-new tools
===================

Here are a few of not-so-new tools folks mentioned aren’t that well known::

    pv (“pipe viewer”, gives you a progress bar for a pipe)
    vidir (from moreutils, lets you batch rename/delete files in vim)
    sponge, ts, parallel (also from moreutils)


some of my favourites
===========================

My favourites of these that I use already are entr, ripgrep, git-delta,
httpie, plocate, and jq.

I’m interested in trying out :ref:`direnv <direnv>`, btm, z, xsv, and duf, but I think
the most exciting tool I learned about is vidir.
