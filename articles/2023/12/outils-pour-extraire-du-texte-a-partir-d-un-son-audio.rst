.. index::
   pair: Transcription; Outils de transcription pour extraire et formater du texte à partir d'un son audio

.. _tools_audio_2023_12_19:

===================================================================================================================================
2023-12-19 **Outils de transcription pour extraire et formater du texte à partir d'un son audio (ffmpeg, audio-to-text, fmt)**
===================================================================================================================================


Pour extraire et formater du texte à partir d'un son audio le travail
j'ai utilisé ces outils:

- ffmpeg (sous Debian) pour découper le fichier audio en plusieurs morceaux
  (pour que chaque morceau fasse moins de 10 minutes)
  Exemple: ffmpeg -i akadem.wav  -f segment -segment_time 540 -c copy "akadem_%03d.wav"
- https://www.veed.io/tools/audio-to-text une application web pour la
  traduction de fichiers audio (il faut que les fichiers fassent moins de 10Mo sinon c'est payant)
- fmt (sous Debian) pour formatter le texte sur n caractères
  fmt -w 82 < t.txt > output.rst (Le texte à formater est dans le fichier
  t.txt. Le texte formaté est dans output.rst)
