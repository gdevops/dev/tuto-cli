

.. _powerline_bash_2020_03_24:

========================================
2020-03-24 actualité de powerline_bash
========================================

.. seealso::

   - https://linuxfr.org/users/bersace/journaux/actualite-de-powerline-bash-optimisations-icones-nouveaux-segments-et-plus


.. contents::
   :depth: 3

Introduction
=============

Salut à tous,

En ces temps de chômage technique, je suis sûr que beaucoup d'entre vous
profite de ce temps pour mouler^Waffuter leur outil favori : BASH !

Voilà pile 2 ans, j'ai commencé le développement de powerline.bash.

Il s'agit d'un fork de l'invite de commande de Sandile Keswa, qui lui
n'a pas bougé depuis 2017.

Mon but premier était d'adopter le style powerline, d'ajouter le virtualenv
Python et autres améliorations.
La première mouture a eut un succès relatif (traduire : étonnamment, je
ne suis pas le seul à l'utiliser).

L'importance de l'invite de commande
========================================

Reconnaissons que ce n'est pas simple de configurer aux petits oignons
une invite de commande.
Et pourtant, cela me semble crucial tant pour un développeur que pour un
administrateur système.

Voici les informations qui me semblent essentielles à connaître au
moment de rédiger une commande :

- qui suis-je ? root ou pas ?
- où suis-je ? quelle machine ? quel dossier ?
- dans quel état j'ère ? la commande précédente a-t-elle planté ?
  Suis-je en cours de rebase ? sur quelle branche git ?
  Ai-je oublié un fichier ?

Ceci permet d'éviter des erreurs du genre :

- commande malheureuse en prod ou en root.
- oubli de sudo entêtant.
- rebasage foiré et perte de commit (avec séjour interminable dans reflog)
- push -f sur master et autres occasions d'offrir les chouquettes le lendemain matin.


Pour soi et pour les autres
===============================

On peut aller plus loin dans la remonté de l'état du terminal :
version de Python, ruby, fichier openrc (OpenStack), état de services
docker-compose, etc.

Les possibilités sont aussi grandes que les applets sur son bureau.

L'invite de commande est importante aussi pour les autres.
Quand est-ce qu'on partage son invite de commande avec son collègue ?
Bien plus souvent qu'on ne le croit ! En copi-collant la sortie d'un
terminal dans un ticket, en partageant sont écran pour faire une
démonstration, en binômant pour chasser un bug.

Autant d'occasion où une invite obscure n'aide pas voire trouble le pair.

C'est dans cette optique que j'apprécie les icônes dans l'invite de
commande. Il permet de manière concise de distinguer que admin@integration
et root@gargamel sont l'un un accès OpenStack et l'autre un accès SSH.

C'est aussi pour les autres que powerline.bash génère une invite sur
deux lignes : la première est une sorte de barre d'état avec des
segments indiquant l'état du terminal.

La deuxième ligne est un simple $ ou # prêt à recevoir la commande.
Votre curseur est toujours à la même place. Cette ligne est très
facilement copi-collable et sur les forges GitHub ou GitLab, le rendu
HTML de markdown console en tient compte.

Simple, rapide, compatible, expressif, joli, extensible

Il y a le choix dans les projets d'invite de commande. Du sapin de Noël
à l'invite sobre, tout est dans le commerce.

J'ai fait powerline.bash pour avoir en un projet :

- Une installation très simple : copier un fichier et le sourcer. pas de serveur.
- Une exécution extrêmement rapide. De l'ordre de 15ms avec exécution
  de git status. Sans git, on descend à 2ms !
- Un rendu élégant avec le style powerline, des coleurs choisies et
  icons-in-terminal.
- Une portabilité : vieux bash, vieux git, pas d'icône ni de chevron Powerline.
- Un large choix d'informations à remonter : sudo, ssh, dossier courant,
  branche et synchronisation amont de dépôt **git, version pyenv, virtualenv python**,
  openrc openstack, nouveaux messages dans maildir.

Tout cela au prix d'une implémentation pleine de bashismes et avec
quelques optimisations obscures.

Collaboration
==============

Le projet est en français à l'instar de Knacss. Vu l'hégémonie de l'anglais
parmi les développeurs français, ça détonne.
C'est d'abord parce que je ne prétends pas être utilisé par la
silicon valley. Ce projet est pour moi, mes pairs.

Récemment, j'ai reçu plusieures contributions majeures de Frédéric MAYNADIER.

Discuter du code en français a été très agréable et très efficace.
Je suis ouvert à accueillir de nouveaux segments : cela permet de partager
du code et de l'optimiser. N'hésitez-pas !

Et vous ? Qu'utilisez-vous ? Partagez-vous votre invite de commande ?
