.. index::
   pair: Tool ; x-pipe
   pair: Your entire server infrastructure at your fingertips ; x-pipe
   ! x-pipe

.. _ref_xpipe:

=====================================================================================
**x-pipe  Your entire server infrastructure at your fingertips**
=====================================================================================

- https://github.com/xpipe-io/xpipe
- :ref:`xpipe`
