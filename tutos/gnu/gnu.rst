.. index::
   pair: Tutorial ; GNU

.. _gnu:

============================================
**Standards for Command Line Interfaces**
============================================

- https://www.gnu.org/prep/standards/html_node/Command_002dLine-Interfaces.html


Description
==============

An open-source guide to help you write better command-line programs, taking
traditional UNIX principles and updating them for the modern day.


Used by
===========

- https://github.com/sphinx-doc/sphinx/pull/11776
