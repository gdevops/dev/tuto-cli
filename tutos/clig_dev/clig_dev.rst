.. index::
   pair: Tutorial ; clig-dev

.. _clig_dev:

============================================
Clig Dev
============================================

- https://clig.dev/


Description
==============

An open-source guide to help you write better command-line programs, taking
traditional UNIX principles and updating them for the modern day.


Used by
===========

- https://github.com/sphinx-doc/sphinx/pull/11776
