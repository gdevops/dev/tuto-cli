
.. _powerline_bash_installation:

============================================================================================================
powerline.bash **installation**
============================================================================================================

.. seealso::

   - https://gitlab.com/bersace/powerline.bash
   - https://linuxfr.org/users/bersace/journaux/actualite-de-powerline-bash-optimisations-icones-nouveaux-segments-et-plus


.. contents::
   :depth: 3

Installation de **powerline.bash** dans ~/.config/
===================================================

::

    $ curl -Lo ~/.config/powerline.bash https://gitlab.com/bersace/powerline-bash/raw/master/powerline.bash



Modifier .bashrc
==================

::

    $ $EDITOR ~/.bashrc
    # Copier et adapter ceci dans votre .bashrc

::

    . ${HOME}/.config/powerline.bash
    PROMPT_COMMAND='__update_ps1 $?'
    POWERLINE_WINDOW_TITLE="\\h $(pwd)"
    # alias t='POWERLINE_WINDOW_TITLE="\\h $(whoami) $(pwd)"'
    alias t='POWERLINE_WINDOW_TITLE="\\h $(pwd)"'


Mettre à jour le titre de la fenêtre de terminal
======================================================

::

    t


A faire quand on change de répertoire.


Mise à jour du shell bash courant
===================================

::

    $ exec $SHELL


.. figure:: terminal_git.png
   :align: center
