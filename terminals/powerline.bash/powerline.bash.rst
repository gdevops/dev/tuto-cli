
.. _powerline_bash:

============================================================================================================
**powerline.bash** (une invite de commande rapide dans le style Powerline, pour BASH)
============================================================================================================

.. seealso::

   - https://gitlab.com/bersace/powerline.bash
   - :ref:`bash`
   - :ref:`powerline_bash_2020_03_24`


.. toctree::
   :maxdepth: 3

   installation/installation
