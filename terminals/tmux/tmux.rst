

.. index::
   pair: tmux; Terminals
   pair: multiplexer; Terminal


.. _tmux:

========================================
tmux is a terminal multiplexer
========================================


.. seealso::

   - https://github.com/tmux/tmux
   - https://doc.ubuntu-fr.org/tmux


.. contents::
   :depth: 3


Official documentation
=========================

.. seealso::

   - https://man.openbsd.org/OpenBSD-current/man1/tmux.1
