

.. index::
   pair: CLI; Terminals

.. _terminals:

========================================
Terminals
========================================


.. toctree::
   :maxdepth: 3

   powerline.bash/powerline.bash
   tmux/tmux
