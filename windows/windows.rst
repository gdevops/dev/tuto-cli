﻿
.. index::
   pair: cli ; Windows


.. _command_line_interface_windows:

===========================================
Windows
===========================================


.. seealso::

   - http://commandwindows.com/index.html
   - http://windows.developpez.com/cours/ligne-commande/
   - http://www.eleves.ens.fr/wintuteurs/advanced/scripting.html


.. toctree::
   :maxdepth: 4

   cmd.exe/cmd.exe
   command.com/command.com
   path_editor
   powershell/powershell
   wmic/wmic
