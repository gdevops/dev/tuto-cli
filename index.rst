.. index::
   pair: Guidelines ; CLI

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>


|FluxWeb| `RSS <https://gdevops.frama.io/dev/tuto-cli/rss.xml>`_


.. _tuto_cli:

============================================
**Command Line Interface (CLI)** tutorial
============================================

- https://en.wikipedia.org/wiki/Command-line_interface
- https://clig.dev/ (Line Interface Guidelines)
- https://command-not-found.com
- https://www.commandlinefu.com/commands/browse
- http://feeds2.feedburner.com/Command-line-fu (flux RSS)
- https://lym.readthedocs.io/en/latest/ (Welcome to Linux command line for you and me!)


.. toctree::
   :maxdepth: 8

   articles/articles
   gnu_linux/gnu_linux
   perl/perl
   python/python
   shells/shells
   terminals/terminals
   tutos/tutos
   tools/tools
   windows/windows
